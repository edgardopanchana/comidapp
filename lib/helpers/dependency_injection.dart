
import 'package:comidapp/api/authentication_api.dart';
import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/api/reservation_api.dart';
import 'package:comidapp/api/user_api.dart';
import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/base_http_dio.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';

abstract class DependencyInjection{

  static void initialize(){
    // Dio dio = Dio(
    //   BaseOptions(baseUrl: 'http://137.184.106.160:8080/api'),
    // );
    Dio dio = Dio(
      BaseOptions(baseUrl: 'http://137.184.106.160:8080/api'),
    );

    BaseHttpDio baseHttpDio = BaseHttpDio(dio, true);
    final AuthenticationAPI authenticationAPI = AuthenticationAPI(baseHttpDio);
    GetIt.instance.registerSingleton<AuthenticationAPI>(authenticationAPI);

    final FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();
    final AuthenticationClient authenticationClient = AuthenticationClient(flutterSecureStorage);
    GetIt.instance.registerSingleton<AuthenticationClient>(authenticationClient);
    
    final FoodAPI localFoodAPI = FoodAPI(baseHttpDio, authenticationClient);
    GetIt.instance.registerSingleton<FoodAPI>(localFoodAPI);

    final UserAPI userAPI = UserAPI(baseHttpDio, authenticationClient);
    GetIt.instance.registerSingleton<UserAPI>(userAPI);

    final EventAPI eventAPI = EventAPI(baseHttpDio, authenticationClient);
    GetIt.instance.registerSingleton<EventAPI>(eventAPI);

    final ReservationAPI reservationAPI = ReservationAPI(baseHttpDio, authenticationClient);
    GetIt.instance.registerSingleton<ReservationAPI>(reservationAPI);

  }
}