import 'package:flutter/material.dart';

class Singleton {
  static final Singleton _backend = Singleton._internal();

  factory Singleton() {
    return _backend;
  }

  Singleton._internal();

  String setTimeToText(TimeOfDay time) {
    if (time == null) {
      return "Select time";
    } else {
      final hours = time.hour.toString().padLeft(2, '0');
      final minutes = time.minute.toString().padLeft(2, '0');
      return '$hours:$minutes';
    }

  }

  String setDateToText(DateTime date) {
    if(date == null) {
      return "Select date";
    }else{
        return'${date.year}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}';
  }
  }


}