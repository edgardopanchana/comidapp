import 'package:comidapp/helpers/base_http_dio.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/authentication_response.dart';
import 'package:comidapp/models/signup_response.dart';

class AuthenticationAPI {
  final BaseHttpDio baseHttpDio;

  AuthenticationAPI(this.baseHttpDio);

  Future<HttpResponse<SignUpResponse>> signUp(
      String email,
      String password,
      String name,
      String lastname,
       String aboutMe,
      String photo,
      String telephone
      ) {
    return baseHttpDio
        .resquest<SignUpResponse>('/auth/signup', method: "POST", data: {
      'email': email,
      'password': password,
      'name': name,
      'last_name': lastname,
      'about_me': aboutMe,
      'photo': photo,
      'direction': "Alguna ciudad del planeta tierra",
      'telephone': telephone
    }, parser: (data) {
      return SignUpResponse.fromJson(data);
    });
  }

  Future<HttpResponse<AuthenticationResponse>> login(
      String email, String password) {
    return baseHttpDio.resquest<AuthenticationResponse>('/auth/signin',
        method: "POST",
        data: {'email': email, 'password': password}, parser: (data) {
      return AuthenticationResponse.fromJson(data);
    });
  }
}
