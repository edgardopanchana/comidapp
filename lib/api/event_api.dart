import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/base_http_dio.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/create_event_response.dart';
import 'package:comidapp/models/event_reponse.dart';
import 'package:comidapp/models/events_response.dart';

class EventAPI {
  final BaseHttpDio baseHttpDio;
  final AuthenticationClient authenticationClient;

  EventAPI(this.baseHttpDio, this.authenticationClient);

  Future<HttpResponse<EventsResponse>> getEvents(int foodID) async {
    final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest('/getEventByFood/' + foodID.toString(),
        method: 'GET',
        headers: {
          "x-access-token":token
        },
        parser: (data){
          return EventsResponse.fromJson(data);
        }
    );
  }

  Future<HttpResponse<EventResponse>> getEventByID(int eventID) async {
    final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest('/getEventById/' + eventID.toString(),
        method: 'GET',
        headers: {
          "x-access-token":token
        },
        parser: (data){
          return EventResponse.fromJson(data);
        }
    );
  }

  Future<HttpResponse<CreateEventResponse>> addEventToFood(
      int foodId,
      String date,
      String hour,
      ) async {
    final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest<CreateEventResponse>('/events',
        method: 'POST',
        data: {
          "foodId": foodId,
          "date": date,
          "hour": hour
        },
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          "x-access-token":token
        },
        parser: (data){
          return CreateEventResponse.fromJson(data);
        }
    );
  }


}