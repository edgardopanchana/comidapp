import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/base_http_dio.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/local_reservations_response.dart';
import 'package:comidapp/models/set_reservation_response.dart';
import 'package:comidapp/models/tourist_reservations_response.dart';

class ReservationAPI {
  final BaseHttpDio baseHttpDio;
  final AuthenticationClient authenticationClient;

  ReservationAPI(this.baseHttpDio, this.authenticationClient);

  //Save a reservation
  Future<HttpResponse<SetReservationResponse>> setReservation(
      String eventId, String message, int guests) async {
    final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest<SetReservationResponse>(
        '/reservations/' + eventId,
        method: 'POST',
        data: {
          "message": message,
          "persons": guests
        },
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          "x-access-token": token
        }, parser: (data) {
      return SetReservationResponse.fromJson(data);
    });
  }


  Future<HttpResponse<TouristReservationResponse>> getReservationsByTouristId(int touristUserId) async {
    final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest('/reservationsByUser/' + touristUserId.toString(),
        method: 'GET',
        headers: {
          "x-access-token":token
        },
        parser: (data){
          return TouristReservationResponse.fromJson(data);
        }
    );
  }

  Future<HttpResponse<LocalReservationResponse>> getReservationsByLocalId(int localUserId) async {
    final token = await authenticationClient.accessToken;
    return baseHttpDio.resquest('/getReservationsByChef/' + localUserId.toString(),
        method: 'GET',
        headers: {
          "x-access-token":token
        },
        parser: (data){
          return LocalReservationResponse.fromJson(data);
        }
    );
  }

}
