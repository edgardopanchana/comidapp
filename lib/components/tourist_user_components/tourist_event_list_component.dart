import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/components/tourist_user_components/tourist_event_item_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/event.dart';
import 'package:comidapp/models/events_response.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class EventListComponent extends StatefulWidget {
  final int idFood, selectedIndex;
  final Function onTap;

  const EventListComponent(
      {Key? key,
      required this.idFood,
      required this.onTap,
      required this.selectedIndex})
      : super(key: key);

  @override
  State<EventListComponent> createState() => _EventListComponentState();
}

class _EventListComponentState extends State<EventListComponent> {
  final EventAPI eventAPI = GetIt.instance<EventAPI>();

  Future<List<Event>> getEvents() async {
    HttpResponse<EventsResponse> response =
        await eventAPI.getEvents(widget.idFood);
    return response.data!.events;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Event>>(
        future: getEvents(),
        builder: (context, snapshot) {
          Widget child = const Text("Connecting");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          if (snapshot.data!.isNotEmpty) {
            child = ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  List<Event>? lista = snapshot.data;

                  return TouristEventItemComponent(
                      event: lista![index],
                      onTap: widget.onTap,
                      index: index,
                      selectedIndex: widget.selectedIndex);
                });
          } else {
            child = const Center(
                child: Text(
              "NO EVENTS COMING",
              style: RegularLargeTextStyle,
            ));
          }
          return Container(
            child: child,
          );
        },
      ),
    );
  }
}
