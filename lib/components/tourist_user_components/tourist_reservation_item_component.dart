import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/components/general_components/complete_event_data_component.dart';
import 'package:comidapp/components/general_components/partial_food_data_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/event_reponse.dart';
import 'package:comidapp/models/food_response.dart';
import 'package:comidapp/models/reservation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class TouristReservationItemComponent extends StatefulWidget {
  final Reservation reservation;
  final Function onTap;

  const TouristReservationItemComponent(
      {Key? key, required this.reservation, required this.onTap})
      : super(key: key);

  @override
  State<TouristReservationItemComponent> createState() =>
      _TouristReservationItemComponentState();
}

class _TouristReservationItemComponentState
    extends State<TouristReservationItemComponent> {
  final EventAPI eventAPI = GetIt.instance<EventAPI>();
  final FoodAPI foodAPI = GetIt.instance<FoodAPI>();

  Future<EventResponse> getEvent() async {
    HttpResponse<EventResponse> response =
        await eventAPI.getEventByID(widget.reservation.eventId);
    return response.data!;
  }

  Future<FoodResponse> getFood() async {
    HttpResponse<FoodResponse> response =
     await foodAPI.getFoodByID(widget.reservation.foodId);
    return response.data!;
  }


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap(widget.reservation);
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PartialFoodDataComponent(foodId: widget.reservation.foodId,),
          CompleteEventDataComponent(eventId: widget.reservation.eventId, color: primaryColor,),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children:  [
                    Row(
                      children: const [
                        Icon(Icons.person,color:primaryColor,),
                        Text(
                          "Guests:",
                          style: RegularMediumTextStyle,
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      widget.reservation.persons.toString(),
                      style: RegularMediumTextStyle,
                    ),
                  ],
                ),
              ]),
        ],
      ),
    );
  }
}
