import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/models/event.dart';
import 'package:flutter/material.dart';

class TouristEventItemComponent extends StatefulWidget {
  final int index, selectedIndex;
  final Event event;
  final Function onTap;

  const TouristEventItemComponent(
      {Key? key,
      required this.event,
      required this.onTap,
      required this.index,
      required this.selectedIndex})
      : super(key: key);

  @override
  State<TouristEventItemComponent> createState() => _EventComponentState();
}

class _EventComponentState extends State<TouristEventItemComponent> {
  Color backgroundColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    final year = widget.event.date.year.toString();
    final month = widget.event.date.month.toString().padLeft(2, '0');
    final day = widget.event.date.day.toString().padLeft(2, '0');

    return Card(
      shadowColor: primaryColor,
      elevation: 10,
      margin: EdgeInsets.all(5),
      child: SizedBox(
        width: 120,
        height: 50,
        child: ListTile(
          title: Text(
            widget.event.hour.toString(),
            style: RegularMediumTextStyle,
          ),
          subtitle: Text(
            "$year-$month-$day",
            style: RegularMediumTextStyle,
          ),
          selected: widget.selectedIndex == widget.index ? true : false,
          selectedTileColor: primaryAccentColor,
          onTap: () {
            setState(() {
              widget.onTap(widget.index, widget.event);
            });
          },
        ),
      ),
    );
  }
}
