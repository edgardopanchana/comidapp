import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/components/general_components/complete_event_data_component.dart';
import 'package:comidapp/components/general_components/partial_user_data_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/event_reponse.dart';
import 'package:comidapp/models/reservation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class LocalReservationItemComponent extends StatefulWidget {
  final Reservation localReservation;
  final Function onTap;

  const LocalReservationItemComponent(
      {Key? key, required this.onTap, required this.localReservation})
      : super(key: key);

  @override
  State<LocalReservationItemComponent> createState() =>
      _LocalReservationItemComponentState();
}

class _LocalReservationItemComponentState
    extends State<LocalReservationItemComponent> {
  final EventAPI eventAPI = GetIt.instance<EventAPI>();

  // final LocalFoodAPI foodAPI = GetIt.instance<LocalFoodAPI>();

  Future<EventResponse> getEvent() async {
    HttpResponse<EventResponse> response =
        await eventAPI.getEventByID(widget.localReservation.eventId);
    return response.data!;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap(widget.localReservation);
      },
      child: Column(
        children: [
          PartialUserDataComponent(userId: widget.localReservation.userId),
          CompleteEventDataComponent(eventId: widget.localReservation.eventId, color: localAccentColor,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:  [
                  Row(
                    children: const [
                      Icon(Icons.person,color:localAccentColor,),
                      Text(
                        "Guests:",
                        style: RegularMediumTextStyle,
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    widget.localReservation.persons.toString(),
                    style: RegularMediumTextStyle,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
