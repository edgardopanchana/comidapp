import 'package:flutter/material.dart';

class BannerImagesComponent extends StatelessWidget {
  const BannerImagesComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Image.asset('assets/images/image_banner_one.jpg', width: size.width),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
          Expanded(child: Image.asset('assets/images/image_banner_two.jpg', height: 80, fit: BoxFit.fill,)),
          Expanded(child: Image.asset('assets/images/image_banner_three.jpg', height: 80, fit: BoxFit.fill,)),
          Expanded(child: Image.asset('assets/images/image_banner_four.jpg', height: 80, fit: BoxFit.fill,)),
        ],)
      ],
    );
  }
}
