import 'package:comidapp/constants/style_constants.dart';
import 'package:flutter/material.dart';

class FoodImageComponent extends StatelessWidget {
  final String price, image;
  const FoodImageComponent({Key? key, required this.price, required this.image}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Image.network(image, fit: BoxFit.fill, width: size.width),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: Container(
            width: size.width,
            color: Colors.black54,
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  r'$'+price.toString(),
                  style: RegularMediumButtonTextStyle,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
