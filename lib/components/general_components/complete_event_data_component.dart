import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/helpers/singleton.dart';
import 'package:comidapp/models/event_reponse.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class CompleteEventDataComponent extends StatelessWidget {
  final int eventId;
  final MaterialColor color;
  CompleteEventDataComponent({Key? key, required this.eventId, required this.color}) : super(key: key);

  final EventAPI eventAPI = GetIt.instance<EventAPI>();

  Future<EventResponse> getEvents() async {
    HttpResponse<EventResponse> response =
    await eventAPI.getEventByID(eventId);
    return response.data!;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<EventResponse>(
      future: getEvents(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
        return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Icon(Icons.lock_clock,color:color,),
                      const Text(
                        "Hour:",
                        style: RegularMediumTextStyle,
                      ),
                    ],
                  ),
                  Row(
                    children:  [
                      Icon(Icons.calendar_today,color:color,),
                      const Text(
                        "Date:",
                        style: RegularMediumTextStyle,
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    snapshot.data!.event.hour.toString(),
                    style: RegularMediumTextStyle,
                  ),
                  Text(
                    Singleton()
                        .setDateToText(snapshot.data!.event.date),
                    style: RegularMediumTextStyle,
                  ),

                ],
              ),
            ]);
      },
    );
  }
}
