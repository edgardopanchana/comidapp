import 'package:comidapp/api/user_api.dart';
import 'package:comidapp/components/general_components/circle_image_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/user.dart';
import 'package:comidapp/models/user_response.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class CompleteUserDataComponent extends StatelessWidget {
  final int userId;

  CompleteUserDataComponent({Key? key, required this.userId}) : super(key: key);

  final UserAPI userAPI = GetIt.instance<UserAPI>();

  Future<User> searchUser() async {
    HttpResponse<UserResponse> response = await userAPI.getUserData(userId);
    return response.data!.user;
  }



  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
      future: searchUser(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CircleImageComponent(
                urlImage: snapshot.data!.photo,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      snapshot.data!.name + " " + snapshot.data!.lastName,
                      style: BoldMediumTextStyle,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            snapshot.data!.aboutMe,
                            style: RegularSmallTextStyle,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      snapshot.data!.email,
                      style: RegularSmallTextStyle,
                    ),
                    Text(
                      snapshot.data!.direction,
                      style: RegularSmallTextStyle,
                    ),
                    Text(
                      "User since: " +
                          snapshot.data!.createdAt.toString().substring(0, 4),
                      style: RegularSmallTextStyle,
                    ),
                     Text(
                        snapshot.data!.telephone),
                  ],
                ),
              ),
            ],
          );
        } else {
          return const Text("NO DATA");
        }
      },
    );
  }
}
