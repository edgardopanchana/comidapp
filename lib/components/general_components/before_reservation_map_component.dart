import 'package:comidapp/constants/style_constants.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class BeforeReservationMapComponent extends StatefulWidget {
  double latitude = 0.0, longitude = 0.0;

  BeforeReservationMapComponent(
      {Key? key, required this.latitude, required this.longitude})
      : super(key: key);

  @override
  State<BeforeReservationMapComponent> createState() => _MapComponentState();
}

class _MapComponentState extends State<BeforeReservationMapComponent> {
  late GoogleMapController googleMapController;
  late CameraPosition initialCameraPosition = CameraPosition(
      target: LatLng(widget.latitude, widget.longitude), zoom: 12);
  Set<Marker> markers = {};

  @override
  void initState() {
    setIcon();
    super.initState();
  }

  void setIcon() async {
    BitmapDescriptor markerbitmap = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(),
      "assets/images/pizza.png",
    );
    markers.add(Marker(
      markerId: const MarkerId("Current position"),
      position: LatLng(widget.latitude, widget.longitude),
      icon: markerbitmap,
    ));

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width,
      height: 250,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: PaddingBetweenWidgets),
        child: GoogleMap(
          initialCameraPosition: initialCameraPosition,
          markers: markers,
          mapToolbarEnabled: false,
          zoomControlsEnabled: false,
          onMapCreated: (GoogleMapController controller) {
            setState(() {
              googleMapController = controller;
            });
          },
          myLocationButtonEnabled: false,
          myLocationEnabled: false,
        ),
      ),
    );
  }
}
