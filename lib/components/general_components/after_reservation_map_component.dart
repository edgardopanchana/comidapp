import 'package:comidapp/constants/style_constants.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AfterReservationMapComponent extends StatefulWidget {
  double latitude=0.0, longitude=0.0;
  AfterReservationMapComponent({Key? key, required this.latitude, required this.longitude}) : super(key: key);

  @override
  State<AfterReservationMapComponent> createState() => _MapComponentState();
}

class _MapComponentState extends State<AfterReservationMapComponent> {

  late GoogleMapController googleMapController;
  late CameraPosition initialCameraPosition = CameraPosition(
      target: LatLng(widget.latitude, widget.longitude), zoom: 12);
  Set<Marker> markers = {};

  @override
  void initState() {
    setIcon();
    super.initState();
  }

  void setIcon() async {
    markers.add(
        Marker(
          markerId: const MarkerId("Current position"),
          position: LatLng(widget.latitude,widget.longitude),
        ));

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width,
      height: 250,

      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: PaddingBetweenWidgets),
        child: GoogleMap(
          initialCameraPosition: initialCameraPosition,
          markers: markers,
          mapToolbarEnabled: true,
          zoomControlsEnabled: true,
          onMapCreated: (GoogleMapController controller) {
            setState(() {
              googleMapController = controller;
            });
          },
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
        ),
      ),
    );
  }
}
