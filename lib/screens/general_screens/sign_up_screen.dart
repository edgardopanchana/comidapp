import 'dart:io';
import 'package:comidapp/api/authentication_api.dart';
import 'package:comidapp/api/firebase_api.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/screens/general_screens/sign_in_screen.dart';
import 'package:comidapp/utils/dialogs.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  final AuthenticationAPI authenticationAPI =
      GetIt.instance<AuthenticationAPI>();
  String email = '', password = '', name = '', lastname = '', aboutMe = '', telephone='';
  UploadTask? task;
  final ImagePicker imagePicker = ImagePicker();
  XFile? imageSelected;
  String path = '';
  String urlDownload = 'NO DATA';

  void selectImage() async {
    imageSelected = await imagePicker.pickImage(source: ImageSource.gallery);
    path = imageSelected!.path;
    setState(() {});
    uploadImage();
  }

  Future uploadImage() async {
    if (imageSelected == null) return;
    final fileName = imageSelected!.path;
    final destination = 'main_image/$fileName';
    File file = File(fileName);
    task = FirebaseAPI.uploadTask(destination, file);
    if (task == null) return;
    final snapshot = await task!.whenComplete(() {});
    urlDownload = await snapshot.ref.getDownloadURL();
    setState(() {});
  }

  Future<void> _submit() async {
    ProgressDialog.show(context);

    final httpResponse = await authenticationAPI.signUp(
        email,
        password,
        name,
        lastname,
        aboutMe,
        urlDownload,
        telephone
    );
    ProgressDialog.dissmiss(context);
    if (httpResponse.data != null) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const SignInScreen()),
        (Route<dynamic> route) => false,
      );
    } else {
      String message = httpResponse.error!.message;
      if (httpResponse.error!.statusCode == -1) {
        message = "No network access";
      } else if (httpResponse.error!.statusCode == 422) {
        message = "User already registered";
      }
      Dialogs.alert(context, "ERROR", message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(PaddingScreen),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(PaddingBetweenWidgets),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                    height: MediaQuery.of(context).size.height * 0.22,
                    child: InkWell(
                      onTap: () {
                        selectImage();
                      },
                      child: Image.file(
                        File(path),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                TextFormField(
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Firstname cannot be empty';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                  onChanged: (value) {
                    name = value;
                  },
                  decoration: textFieldDecoration.copyWith(
                      hintText: 'Enter your firstname'),
                ),
                const SizedBox(
                  height: PaddingBetweenWidgets,
                ),
                TextFormField(
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Lastname cannot be empty';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                  onChanged: (value) {
                    lastname = value;
                  },
                  decoration: textFieldDecoration.copyWith(
                      hintText: 'Enter your lastname'),
                ),
                const SizedBox(
                  height: PaddingBetweenWidgets,
                ),
                TextFormField(
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$')
                            .hasMatch(value)) {
                      return "Email incorrect format";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.name,
                  onChanged: (value) {
                    email = value;
                  },
                  decoration: textFieldDecoration.copyWith(
                      hintText: 'Enter your email'),
                ),

                const SizedBox(
                  height: PaddingBetweenWidgets,
                ),

                TextFormField(
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Telephone cannot be empty';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.phone,
                  onChanged: (value) {
                    telephone = value;
                  },
                  decoration: textFieldDecoration.copyWith(
                      hintText: 'Enter your telephone number'),
                ),

                const SizedBox(
                  height: PaddingBetweenWidgets,
                ),
                TextFormField(
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'About me cannot be empty';
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.multiline,
                  maxLines: 2,
                  decoration:
                      textFieldDecoration.copyWith(hintText: 'About me ...'),
                  onChanged: (value) {
                    aboutMe = value;
                  },
                ),
                const SizedBox(
                  height: PaddingBetweenWidgets,
                ),
                TextFormField(
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Password cannot be empty';
                    } else {
                      return null;
                    }
                  },
                  obscureText: true,
                  onChanged: (value) {
                    password = value;
                  },
                  decoration: textFieldDecoration.copyWith(
                      hintText: 'Enter your password'),
                ),
                const SizedBox(
                  height: PaddingBetweenWidgets,
                ),
                TextFormField(
                  validator: (value) {
                    if (password != value) {
                      return "Password do not match";
                    } else {
                      return null;
                    }
                  },
                  obscureText: true,
                  onChanged: (value) {
                    //password = value;
                  },
                  decoration: textFieldDecoration.copyWith(
                      hintText: 'Confirm your password'),
                ),
                const SizedBox(
                  height: PaddingBetweenWidgets,
                ),
                TextButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate() == true) {
                        _submit();
                      }
                    },
                    child: const Text(
                      'Sign Up',
                      style: RegularMediumButtonTextStyle,
                    ),
                    style: TouristButtonStyle),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Already have an account?",
                        style: RegularSmallTextStyle),
                    TextButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return const SignInScreen();
                          }));
                        },
                        child: const Text("Sign In",
                            style: BoldSmallButtonTextStyle))
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
