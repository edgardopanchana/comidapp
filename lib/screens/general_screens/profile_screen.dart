import 'dart:io';
import 'package:comidapp/api/firebase_api.dart';
import 'package:comidapp/api/user_api.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/user.dart';
import 'package:comidapp/models/user_response.dart';
import 'package:comidapp/screens/tourist_screens/tourist_main_screen.dart';
import 'package:comidapp/utils/dialogs.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final AuthenticationClient authenticationClient = GetIt.instance<AuthenticationClient>();
  final UserAPI userAPI = GetIt.instance<UserAPI>();
  String email = '', password = '', name = '', lastname = '', aboutMe = '', telephone='', direction='';

  final ImagePicker imagePicker = ImagePicker();
  XFile? imageSelected;
  String path = '';
  UploadTask? task;
  String urlDownload='NO DATA';

  Future<User> searchUser() async {
    final userId = await authenticationClient.accessUserId;
    HttpResponse<UserResponse> response =
    await userAPI.getUserData(userId!);
    return response.data!.user;
  }

  Future<void> selectImage() async {
    imageSelected = await imagePicker.pickImage(source: ImageSource.gallery);
    if(imageSelected == null) return;
    final fileName = imageSelected!.path;
    final destination = 'profile_image/$fileName';

    File file = File(imageSelected!.path);
    task = FirebaseAPI.uploadTask(destination, file);

    if(task==null) return;

    final snapshot = await task!.whenComplete((){});
    urlDownload = await snapshot.ref.getDownloadURL();
  }

  Future<void> _submit() async {
    final userId = await authenticationClient.accessUserId;

    ProgressDialog.show(context);

    final httpResponse = await userAPI.editProfileData(
        userId!,
        name,
        lastname,
        aboutMe,
        urlDownload,
        direction,
        telephone
    );
    ProgressDialog.dissmiss(context);

    if (httpResponse.data != null) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => TouristMainScreen(indexOutside: 2)),
            (Route<dynamic> route) => false,
      );
    } else {
      String message = httpResponse.error!.message;
      if (httpResponse.error!.statusCode == -1) {
        message = "No network access";
      } else if (httpResponse.error!.statusCode == 422) {
        message = "User already registered";
      }
      Dialogs.alert(context, "ERROR", message);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: FutureBuilder<User>(
          future: searchUser(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              urlDownload = snapshot.data!.photo;
              return Padding(
                padding: const EdgeInsets.all(6.0),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        InkWell(
                          onTap: ()  {
                            selectImage();
                          },
                          child: Image.network(
                              urlDownload,
                              fit: BoxFit.fill,
                              width: size.width),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          left: 0,
                          child: Container(
                            width: size.width,
                            color: Colors.white70,
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  snapshot.data!.email,
                                  style: RegularMediumTextStyle,
                                ),
                                Text(
                                  snapshot.data!.direction,
                                  style: RegularMediumTextStyle,
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(PaddingBetweenWidgets),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children:[
                            TextFormField(
                              initialValue: snapshot.data!.name,
                              textAlign: TextAlign.start,
                              onChanged: (value){
                                  name = value;
                              },
                              decoration: textFieldDecoration.copyWith(hintText: 'Firstname'),
                            ),
                            const SizedBox(height: PaddingBetweenWidgets),
                            TextFormField(
                              initialValue: snapshot.data!.lastName,
                              textAlign: TextAlign.start,
                              onChanged: (value){
                                lastname = value;
                              },
                              decoration: textFieldDecoration.copyWith(hintText: 'Lastname'),
                            ),
                            const SizedBox(height: PaddingBetweenWidgets),
                            TextFormField(
                              initialValue: snapshot.data!.direction,
                              textAlign: TextAlign.start,
                              onChanged: (value){
                                direction = value;
                              },
                              decoration: textFieldDecoration.copyWith(hintText: 'Address'),
                            ),
                            const SizedBox(height: PaddingBetweenWidgets),
                            TextFormField(
                              initialValue: snapshot.data!.telephone,
                              textAlign: TextAlign.start,
                              keyboardType: TextInputType.phone,
                              onChanged: (value){
                                direction = value;
                              },
                              decoration: textFieldDecoration.copyWith(hintText: 'Telephone'),
                            ),
                            const SizedBox(height: PaddingBetweenWidgets),
                            TextFormField(
                              initialValue: snapshot.data!.aboutMe,
                              keyboardType: TextInputType.multiline,
                              textAlign: TextAlign.start,
                              maxLines: 4,
                              onChanged: (value){
                                aboutMe = value;
                              },
                              decoration: textFieldDecoration.copyWith(hintText: 'About me ...'),
                            ),
                            const SizedBox(height: PaddingBetweenWidgets),
                            TextButton(
                                onPressed: () {
                                  _submit();
                                },
                                child: const Text(
                                  'Done',style: RegularMediumButtonTextStyle,
                                ),
                                style: TouristButtonStyle
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return const Text("NO DATA");
            }
          },
        ),
      ),
    );
  }
}
