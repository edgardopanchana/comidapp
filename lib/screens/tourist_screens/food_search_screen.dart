import 'package:comidapp/components/general_components/banner_image_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:flutter/material.dart';
import 'food_search_result_screen.dart';

class FoodSearchScreen extends StatefulWidget {
  const FoodSearchScreen({Key? key}) : super(key: key);

  @override
  State<FoodSearchScreen> createState() => _FoodSearchScreenState();
}

class _FoodSearchScreenState extends State<FoodSearchScreen> {
  String cityNameToSearch = '';
  bool _validate = false;
  final _cityName = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const BannerImagesComponent(),
            Padding(
              padding: const EdgeInsets.all(PaddingScreen),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                   Padding(
                    padding: const EdgeInsets.symmetric(vertical: PaddingBetweenWidgets),
                    child: Row(
                      children: const [
                        Text("Welcome to ",
                            style: RegularMediumTextStyle),
                        Text("ComidApp",
                            style: BoldLargeTextStyle),
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: PaddingBetweenWidgets),
                    child: Text("Find local food made by locals!",
                        style: RegularMediumTextStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: PaddingBetweenWidgets),
                    child: TextField(
                      controller: _cityName,
                      textAlign: TextAlign.start,
                      keyboardType: TextInputType.text,
                      onChanged: (value) {
                        cityNameToSearch = value;
                      },
                      decoration: textFieldDecoration.copyWith(
                          hintText: 'What city will you visit?',
                          errorText: _validate ? 'City name cannot be empty' : null,
                          suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  _cityName.text.isEmpty ? _validate = true : _validate = false;
                                });
                                if(_validate==false)
                                  {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                          return FoodSearchResultScreen(
                                            cityNameToSearch: cityNameToSearch,
                                          );
                                        }));
                                  }
                              },
                              icon: const Icon(Icons.search))),
                    ),
                  ),
                ],
              ),
            ),
          ]),
    );
  }
}
