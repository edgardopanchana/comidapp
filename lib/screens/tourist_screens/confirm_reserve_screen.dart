import 'package:comidapp/api/reservation_api.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/event.dart';
import 'package:comidapp/models/food.dart';
import 'package:comidapp/models/set_reservation_response.dart';
import 'package:comidapp/screens/tourist_screens/tourist_main_screen.dart';
import 'package:comidapp/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class ConfirmReserveScreen extends StatefulWidget {
  final Food food;
  final Event event;

  const ConfirmReserveScreen(
      {Key? key, required this.food, required this.event})
      : super(key: key);

  @override
  State<ConfirmReserveScreen> createState() => _ConfirmReserveScreenState();
}

class _ConfirmReserveScreenState extends State<ConfirmReserveScreen> {
  int guests = 1;
  double total = 0;
  String messageToLocal='';
  final ReservationAPI reservationAPI = GetIt.instance<ReservationAPI>();

  @override
  void initState() {
    sumTotal();
    super.initState();
  }

  void sumTotal(){
    total = double.parse(widget.food.pvp) * guests;
  }

  void addGuest() {
    if(guests <= widget.food.maxTourist){
      setState(() {
        guests += 1;
        // total = double.parse(widget.food.pvp) * guests;
        sumTotal();
      });
    }else{
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(const SnackBar(content: Text("Maximum number of guests allowed")));
    }
  }

  void deleteGuest() {
    if (guests > 1) {
      setState(() {
        guests -= 1;
        // total = double.parse(widget.food.pvp) * guests;
        sumTotal();
      });
    }
  }

  Future<void> _submit() async {
    ProgressDialog.show(context);
    final HttpResponse<SetReservationResponse> httpResponse = await reservationAPI.setReservation(
        widget.event.id.toString(),
        messageToLocal,
        guests
    );
    ProgressDialog.dissmiss(context);
    if(httpResponse.data != null){
      //log(httpResponse.data.toString());
      await Dialogs.alert(context, "Evento reservado", httpResponse.data!.message);
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return TouristMainScreen(indexOutside: 1);
      }));
    }else
    {
      String message = httpResponse.error!.message;
      if(httpResponse.error!.statusCode == 500){
        message = "No network access";
      }else if (httpResponse.error!.statusCode == 422){
        message = "Error de validación de datos";
      }
      Dialogs.alert(context, "ERROR", message);
    }
  }

  @override
  Widget build(BuildContext context) {
    final year = widget.event.date.year.toString();
    final month = widget.event.date.month.toString().padLeft(2, '0');
    final day = widget.event.date.day.toString().padLeft(2, '0');

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                const Text(
                  "Guests",
                  style: RegularLargeTextStyle,
                ),
                TextButton(
                  onPressed: () {
                    addGuest();
                  },
                  child: const Text(
                    '+',
                    style: BoldLargeTextStyle,
                  ),
                ),
                Text(
                  guests.toString(),
                  style: RegularLargeTextStyle,
                ),
                TextButton(
                  onPressed: () {
                    deleteGuest();
                  },
                  child: const Text(
                    '-',
                    style: BoldLargeTextStyle,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                const Text(
                  "Date:",
                  style: RegularLargeTextStyle,
                ),
                  Text("$year-$month-$day",
                  style: RegularLargeTextStyle,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                const Text(
                  "Time:",
                  style: RegularLargeTextStyle,
                ),
                Text(
                  widget.event.hour.toString(),
                  style: RegularLargeTextStyle,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                const Text(
                  "Total:",
                  style: BoldLargeTextStyle,
                ),
                Text(
                  r'$' + total.toString(),
                  style: BoldLargeTextStyle,
                ),
              ],
            ),
            TextField(
              keyboardType: TextInputType.multiline,
              textAlign: TextAlign.start,
              maxLines: 4,
              decoration: textFieldDecoration.copyWith(
                  hintText: 'Send a message to the local ...'),
              onChanged: (value){
                messageToLocal = value;
              },
            ),
            TextButton(
                onPressed: () {
                  _submit();

                },
                child: const Text(
                  'Confirm reservation',
                  style: RegularMediumButtonTextStyle,
                ),
                style: TouristButtonStyle),
          ],
        ),
      ),
    );
  }
}
