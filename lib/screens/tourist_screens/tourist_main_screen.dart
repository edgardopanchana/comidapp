import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/screens/tourist_screens/tourist_configuration_screen.dart';
import 'package:comidapp/screens/tourist_screens/food_search_screen.dart';
import 'package:comidapp/screens/tourist_screens/tourist_reservations_screen.dart';
import 'package:flutter/material.dart';

class TouristMainScreen extends StatefulWidget {
   int indexOutside = 0;
   TouristMainScreen({Key? key, required this.indexOutside}) : super(key: key);

  @override
  State<TouristMainScreen> createState() => _TouristMainScreenState();
}

class _TouristMainScreenState extends State<TouristMainScreen> {
  int _selectedIndex = 0;

  final screens = [
    const FoodSearchScreen(),
    const TouristReservationsScreen(),
    const ConfigurationScreen()
  ];

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.indexOutside;
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.fastfood),
            label: 'Reserved foods',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Configuration',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        backgroundColor: primaryColor,
        onTap: _onItemTapped,
      ),
    );
  }
}
