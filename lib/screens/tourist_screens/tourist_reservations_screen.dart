import 'package:comidapp/api/reservation_api.dart';
import 'package:comidapp/components/tourist_user_components/tourist_reservation_item_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/reservation.dart';
import 'package:comidapp/models/tourist_reservations_response.dart';
import 'package:comidapp/screens/tourist_screens/tourist_reservation_detail.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class TouristReservationsScreen extends StatefulWidget {
  const TouristReservationsScreen({Key? key}) : super(key: key);

  @override
  State<TouristReservationsScreen> createState() => _ReservedFoodsScreenState();
}

class _ReservedFoodsScreenState extends State<TouristReservationsScreen> {
  final ReservationAPI reservationAPI = GetIt.instance<ReservationAPI>();
  final AuthenticationClient authenticationClient = GetIt.instance<AuthenticationClient>();

  Future<List<Reservation>> getReservations() async {
    final userId = await authenticationClient.accessUserId;
    HttpResponse<TouristReservationResponse> response =
        await reservationAPI.getReservationsByTouristId(userId!);
    return response.data!.reservations;
  }

  void showReservationDetail(Reservation reservation) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return TouristReservationDetail(reservation: reservation,);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Reservation>>(
        future: getReservations(),
        builder: (context, snapshot) {
          Widget child = const Text("Connecting");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          if (snapshot.data!.isNotEmpty) {
            child = ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  List<Reservation>? lista = snapshot.data;
                  return Container(
                    margin: const EdgeInsets.all(12),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 12, vertical: 12),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      boxShadow: [
                        BoxShadow(
                            color: primaryAccentColor, offset: Offset(4, 4))
                      ],
                    ),
                    child: TouristReservationItemComponent(
                      reservation: lista![index],
                      onTap: showReservationDetail,
                    ),
                  );
                });
          } else {
            child = const Center(child: Text("NO RESERVATIONS TO DO", style: RegularLargeTextStyle,));
          }
          return Container(
            child: child,
          );
        },
      ),
    );
  }
}
