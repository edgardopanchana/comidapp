import 'package:comidapp/api/event_api.dart';
import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/components/general_components/complete_event_data_component.dart';
import 'package:comidapp/components/general_components/partial_food_data_component.dart';
import 'package:comidapp/components/general_components/complete_user_data_component.dart';
import 'package:comidapp/components/general_components/after_reservation_map_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/food.dart';
import 'package:comidapp/models/food_response.dart';
import 'package:comidapp/models/reservation.dart';
import 'package:comidapp/screens/tourist_screens/tourist_main_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class TouristReservationDetail extends StatefulWidget {
  final Reservation reservation;

  const TouristReservationDetail({Key? key, required this.reservation})
      : super(key: key);

  @override
  State<TouristReservationDetail> createState() =>
      _TouristReservationDetailState();
}

class _TouristReservationDetailState extends State<TouristReservationDetail> {
  final EventAPI eventAPI = GetIt.instance<EventAPI>();

  final FoodAPI localFoodAPI = GetIt.instance<FoodAPI>();

  Future<FoodResponse> getFood() async {
    HttpResponse<FoodResponse> response =
        await localFoodAPI.getFoodByID(widget.reservation.foodId);
    return response.data!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(PaddingScreen),
        child: Column(
          children: [
            FutureBuilder<FoodResponse>(
              future: getFood(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator.adaptive(),
                  );
                }
                Food? food = snapshot.data!.food;
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PartialFoodDataComponent(
                      foodId: food.id,
                    ),
                    AfterReservationMapComponent(
                      longitude: double.parse(food.longitude),
                      latitude: double.parse(food.latitude),
                    ),
                    CompleteUserDataComponent(userId: food.userId),
                  ],
                );
              },
            ),

            CompleteEventDataComponent(eventId: widget.reservation.eventId, color: primaryColor,),

            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:  [
                      Row(
                        children: const [
                          Icon(Icons.person,color:primaryColor,),
                          Text(
                            "Guests:",
                            style: RegularMediumTextStyle,
                          ),
                        ],
                      ),
                      Row(
                        children: const [
                          Icon(Icons.money,color:primaryColor,),
                          Text(
                            "Paid:",
                            style: RegularMediumTextStyle,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [

                      Text(
                        widget.reservation.persons.toString(),
                        style: RegularMediumTextStyle,
                      ),
                      const Icon(Icons.check,color:primaryColor,),
                    ],
                  ),
                ]),

            TextButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return TouristMainScreen(indexOutside: 1);
                  }));
                },
                child: const Text(
                  'Done',
                  style: RegularMediumButtonTextStyle,
                ),
                style: TouristButtonStyle),
          ],
        ),
      ),
    );
  }
}
