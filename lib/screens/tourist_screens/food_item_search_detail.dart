import 'package:comidapp/api/user_api.dart';
import 'package:comidapp/components/general_components/food_image_component.dart';
import 'package:comidapp/components/general_components/partial_user_data_component.dart';
import 'package:comidapp/components/tourist_user_components/tourist_event_list_component.dart';
import 'package:comidapp/components/general_components/before_reservation_map_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/event.dart';
import 'package:comidapp/models/user.dart';
import 'package:comidapp/models/user_response.dart';
import 'package:comidapp/screens/tourist_screens/confirm_reserve_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:comidapp/models/food.dart';

class FoodItemSearchDetail extends StatefulWidget {
  final Food food;

  const FoodItemSearchDetail({Key? key, required this.food}) : super(key: key);

  @override
  State<FoodItemSearchDetail> createState() => _FoodItemSearchDetailState();
}

class _FoodItemSearchDetailState extends State<FoodItemSearchDetail> {
  final UserAPI userAPI = GetIt.instance<UserAPI>();
  int selectedEventIndex = -1;
  late Event eventSelected;

  void selectEventItem(int index, Event event) {
    selectedEventIndex = index;
    eventSelected = event;
    setState(() {});
  }

  Future<User> searchUser() async {
    HttpResponse<UserResponse> response =
        await userAPI.getUserData(widget.food.userId);
    return response.data!.user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            FoodImageComponent(
              price: widget.food.pvp,
              image: widget.food.image,
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    widget.food.name,
                    style: BoldLargeTextStyle,
                  ),
                  Text(
                    widget.food.city,
                    style: BoldSmallTextStyle,
                  ),
                  Text(
                    widget.food.description,
                    style: RegularMediumTextStyle,
                  ),
                ],
              ),
            ),
            PartialUserDataComponent(userId:widget.food.userId),

            BeforeReservationMapComponent(
              latitude: double.parse(widget.food.latitude),
              longitude: double.parse(widget.food.longitude),
            ),

            SizedBox(
                height: 80,
                width: MediaQuery.of(context).size.width,
                child: EventListComponent(
                  idFood: widget.food.id,
                  onTap: selectEventItem,
                  selectedIndex: selectedEventIndex,
                )),
            //BOTÓN DE RESERVA
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              child: TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return ConfirmReserveScreen(
                        food: widget.food, event: eventSelected,
                      );
                    }));
                  },
                  child: const Text(
                    'Reserve',
                    style: RegularMediumButtonTextStyle,
                  ),
                  style: TouristButtonStyle),
            ),
          ],
        ),
      ),
    );
  }
}
