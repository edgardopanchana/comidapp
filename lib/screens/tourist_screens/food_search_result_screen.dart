import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/components/general_components/food_item_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/foods_response.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'food_item_search_detail.dart';
import 'package:comidapp/models/food.dart';

class FoodSearchResultScreen extends StatefulWidget {
  final String cityNameToSearch;

  const FoodSearchResultScreen({Key? key, required this.cityNameToSearch})
      : super(key: key);

  @override
  State<FoodSearchResultScreen> createState() => _FoodSearchResultScreenState();
}

class _FoodSearchResultScreenState extends State<FoodSearchResultScreen> {
  final FoodAPI localFoodAPI = GetIt.instance<FoodAPI>();

  Future<List<Food>> searchFoods() async {
    HttpResponse<FoodsResponse> response = await localFoodAPI
        .getFoodsbyCityName(widget.cityNameToSearch);
    return response.data!.foods;
  }

  void showFoodItemDetail(Food food) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return FoodItemSearchDetail(food: food,);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<Food>>(
        future: searchFoods(),
        builder: (context, snapshot) {
          Widget child = const Text("Connecting");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          if (snapshot.data!.isNotEmpty) {
            child = ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  List<Food>? lista = snapshot.data;
                  return Container(
                    margin: const EdgeInsets.all(PaddingScreen),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 12, vertical: 12),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      boxShadow: [
                        BoxShadow(
                            color: primaryColor, offset: Offset(4, 4))
                      ],
                    ),
                    child: FoodItemComponent(
                      food: lista![index], onTap: showFoodItemDetail,),
                  );
                });
          }
          else {
            child = const Padding(
              padding: EdgeInsets.all(PaddingScreen),
              child: Center(child: Text("NO FOOD RESULTS, TRY WITH OTHER CITY", style: RegularLargeTextStyle,)),
            );
          }
          return Container(
            child: child,
          );
        },
      ),
    );
  }
}