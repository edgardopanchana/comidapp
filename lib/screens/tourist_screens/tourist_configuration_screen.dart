import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/data/authentication_client.dart';
import 'package:comidapp/screens/local_screens/local_main_screen.dart';
import 'package:comidapp/screens/general_screens/profile_screen.dart';
import 'package:comidapp/screens/general_screens/sign_in_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class ConfigurationScreen extends StatefulWidget {
  const ConfigurationScreen({Key? key}) : super(key: key);

  @override
  State<ConfigurationScreen> createState() => _ConfigurationScreenState();
}

class _ConfigurationScreenState extends State<ConfigurationScreen> {
  final AuthenticationClient authenticationClient = GetIt.instance<AuthenticationClient>();

  Future<void> signOut() async {
    await authenticationClient.LogOut();
    Navigator.push(context,
        MaterialPageRoute(builder: (context) {
          return const SignInScreen();
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(Icons.sync),
              TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                          return LocalMainScreen(indexOutside: 0,);
                        }));
                  },
              child: const Text('Change to local mode', style: LocalBoldMediumButtonTextStyle, )),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(Icons.verified_user),
              TextButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) {
                        return const ProfileScreen();
                      }));
                },
                child:
                const Text('My profile', style: RegularMediumTextStyle,)),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(Icons.food_bank),
              TextButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) {
                        return LocalMainScreen(indexOutside: 0,);
                      }));
                },
                child:
                const Text('User guide', style: RegularMediumTextStyle,)),
            ],
          )
          ,
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Icon(Icons.door_back_door),
              TextButton(
                onPressed: signOut,
                child:
                const Text('Log out', style: RegularMediumTextStyle,)),
            ],
          )
        ],
      ),
    );
  }
}
