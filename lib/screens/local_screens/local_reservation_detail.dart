import 'package:comidapp/components/general_components/complete_event_data_component.dart';
import 'package:comidapp/components/general_components/complete_user_data_component.dart';
import 'package:comidapp/components/general_components/partial_food_data_component.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/models/reservation.dart';
import 'package:flutter/material.dart';
import 'local_main_screen.dart';

class LocalReservationDetail extends StatefulWidget {
  final Reservation localReservation;

  const LocalReservationDetail({Key? key, required this.localReservation}) : super(key: key);

  @override
  State<LocalReservationDetail> createState() => _ReservationItemDetailState();
}

class _ReservationItemDetailState extends State<LocalReservationDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(PaddingScreen),
        child: Column(

          children: [

            const SizedBox(height: PaddingScreen,),
            CompleteUserDataComponent(userId: widget.localReservation.userId),

            const SizedBox(height: PaddingScreen,),
            PartialFoodDataComponent(foodId: widget.localReservation.foodId,),

            const SizedBox(height: PaddingScreen,),
            CompleteEventDataComponent(eventId: widget.localReservation.eventId, color: localAccentColor),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                    children: [

                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:  [
                          Row(
                            children: const [
                              Icon(Icons.person,color:localAccentColor,),
                              Text(
                                "Guests:",
                                style: RegularMediumTextStyle,
                              ),
                            ],
                          ),
                          Row(
                            children: const [
                              Icon(Icons.money,color:localAccentColor,),
                              Text(
                                "Paid:",
                                style: RegularMediumTextStyle,
                              ),
                            ],
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [

                          Text(
                            widget.localReservation.persons.toString(),
                            style: RegularMediumTextStyle,
                          ),
                          const Icon(Icons.check,color:localAccentColor,),
                        ],
                      ),
                    ]),

            const SizedBox(height: PaddingScreen,),
            Text(widget.localReservation.message),

            const SizedBox(height: PaddingScreen,),
            TextButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LocalMainScreen(indexOutside: 1);
                  }));
                },
                child: const Text(
                  'Done',style: RegularMediumButtonTextStyle,
                ),
                style: LocalButtonStyle
            ),
          ],
        ),
      ),
    );
  }
}
