import 'dart:io';
import 'package:comidapp/api/firebase_api.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UploadFoodPictures extends StatefulWidget {
  const UploadFoodPictures({Key? key}) : super(key: key);

  @override
  State<UploadFoodPictures> createState() => _UploadFoodPicturesState();
}

class _UploadFoodPicturesState extends State<UploadFoodPictures> {
  UploadTask? task;
  final ImagePicker imagePicker = ImagePicker();
  XFile? imageSelected;
  String path = '';
  String urlDownload='NO DATA';

  void selectImage() async {
    imageSelected = await imagePicker.pickImage(source: ImageSource.gallery);
    path = imageSelected!.path;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(PaddingScreen),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              OutlinedButton(
                  onPressed: () {
                    selectImage();
                  },
                  child: const Text("Choose image")),
              Expanded(
                  child:Image.file(File(path), fit: BoxFit.cover,)
              ),
              const SizedBox(height: PaddingBetweenWidgets,),
              ElevatedButton(
                  onPressed: () {
                      uploadImage();
                  },
                  style: LocalButtonStyle,
                  child: const Text("Upload Image",style: RegularMediumButtonTextStyle)),
              const SizedBox(height: PaddingBetweenWidgets,),
              Text(urlDownload),
              const SizedBox(height: PaddingBetweenWidgets,),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context, urlDownload);
                  },
                  style: LocalButtonStyle,
                  child: const Text("SET",style: RegularMediumButtonTextStyle)),
            ],
          ),
        ),
      ),
    );
  }

  Future uploadImage() async {
    if(imageSelected == null) return;
    final fileName = path;
    final destination = 'main_image/$fileName';
    File file = File(imageSelected!.path);
    task = FirebaseAPI.uploadTask(destination, file);
    if(task==null) return;
    final snapshot = await task!.whenComplete((){});
    urlDownload = await snapshot.ref.getDownloadURL();
    setState(() {});
  }

}
