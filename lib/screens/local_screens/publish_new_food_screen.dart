import 'package:comidapp/api/food_api.dart';
import 'package:comidapp/constants/style_constants.dart';
import 'package:comidapp/helpers/http_response.dart';
import 'package:comidapp/models/create_food_response.dart';
import 'package:comidapp/screens/local_screens/create_events_screen.dart';
import 'package:comidapp/screens/local_screens/set_location_on_map_screen.dart';
import 'package:comidapp/screens/local_screens/upload_food_pictures.dart';
import 'package:comidapp/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';

class PublishNewFoodScreen extends StatefulWidget {
  const PublishNewFoodScreen({Key? key}) : super(key: key);

  @override
  State<PublishNewFoodScreen> createState() => _PublishNewFoodScreenState();
}

class _PublishNewFoodScreenState extends State<PublishNewFoodScreen> {
  final FoodAPI localFoodAPI = GetIt.instance<FoodAPI>();
  late String title='', description='', city='';
  late int pvp=0, maxTourist=0;
  String? longitude, latitude;
  String? urlImage;

  Future<void> _submit() async {
    ProgressDialog.show(context);
    final HttpResponse<CreateFoodResponse> httpResponse = await localFoodAPI.createFood(
        title,
        description,
        pvp.toString(),
        longitude!,
        latitude!,
        city,
      maxTourist.toString(),
      urlImage!
    );
    ProgressDialog.dissmiss(context);
    if(httpResponse.data != null){
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => CreateEventsScreen(foodId:httpResponse.data!.foodId)),
            (Route<dynamic> route) => false,
      );
    }else
    {
      String message = httpResponse.error!.message;
      if(httpResponse.error!.statusCode == 500){
        message = "No network access";
      }else if (httpResponse.error!.statusCode == 422){
        message = "Error de validación de datos";
      }
      Dialogs.alert(context, "ERROR", message);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child:
        Padding(
          padding: const EdgeInsets.all(PaddingScreen),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:[
                TextField(
                  onChanged: (value) {
                    title = value;
                  },
                  decoration: localTextFieldDecoration.copyWith(hintText: 'Title'),
                ),
                const SizedBox(height: PaddingBetweenWidgets,),
                TextField(
                  onChanged: (value) {
                    city = value;
                  },
                  decoration: localTextFieldDecoration.copyWith(hintText: 'City'),
                ),
                const SizedBox(height: PaddingBetweenWidgets,),
                TextField(
                  keyboardType: TextInputType.multiline,
                  textAlign: TextAlign.start,
                  maxLines: 4,
                  enableSuggestions: false,
                  autocorrect: false,
                  onChanged: (value) {
                    description = value;
                  },
                  decoration: localTextFieldDecoration.copyWith(hintText: 'Description'),
                ),
                const SizedBox(height: PaddingBetweenWidgets,),
                TextField(
                  onChanged: (value) {
                    maxTourist = int.parse(value);
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: localTextFieldDecoration.copyWith(hintText: 'Max tourist per event'),
                ),
                const SizedBox(height: PaddingBetweenWidgets,),
                TextField(
                  onChanged: (value) {
                    pvp = int.parse(value);
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: localTextFieldDecoration.copyWith(hintText: 'Price'),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    TextButton(
                        onPressed: () {
                          _setLocationOnMap(context);
                        },
                        child:
                        const Text('Set location on map', style: RegularMediumTextStyle,)),
                    const Icon(Icons.arrow_right),
                  ],
                ),

                Padding(
                  padding: const EdgeInsets.only(left:PaddingBetweenWidgets),
                  child: Row(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text("Longitude:"),
                          Text("Latitude:"),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(longitude ?? "No longitude data"),
                            Text(latitude ?? "No latitude data"),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    TextButton(
                        onPressed: () {
                          _setFoodImage(context);
                        },
                        child:
                        const Text('Set main picture', style: RegularMediumTextStyle,)),
                    const Icon(Icons.arrow_right),
                  ],
                ),

                const SizedBox(height: PaddingBetweenWidgets,),

                Padding(
                  padding: const EdgeInsets.only(left:PaddingBetweenWidgets),
                  child: Column(
                    children: [
                      Text("URL Image:"),
                      Text(urlImage ?? "No URL data", overflow: TextOverflow.ellipsis,),
                    ],
                  ),
                ),

                const SizedBox(height: PaddingBetweenWidgets,),
                Center(
                  child: TextButton(
                      onPressed: () {
                        _submit();
                      },
                      child: const Text(
                        'SAVE',style: RegularMediumButtonTextStyle,
                      ),
                      style: LocalButtonStyle
                  ),
                ),
              ]
          ),
        ),
      ),
    );
  }

  void _setLocationOnMap(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const SetLocationOnMapScreen()),
    );

    setState(() {
      longitude = result[0];
      latitude = result[1];
    });

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(const SnackBar(content: Text("Longitude and Latitude set")));
  }

  void _setFoodImage(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const UploadFoodPictures()),
    );

    setState(() {
      urlImage = result;
    });

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text("URL set")));
  }

}
