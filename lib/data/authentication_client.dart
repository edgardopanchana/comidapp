import 'dart:convert';
import 'package:comidapp/models/authentication_response.dart';
import 'package:comidapp/models/session.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthenticationClient{
  final FlutterSecureStorage flutterSecureStorage;
  AuthenticationClient(this.flutterSecureStorage);

  Future<String?> get accessToken async{
    final data = await flutterSecureStorage.read(key: 'session');
    if(data!=null){
     final session = Session.fromJson(jsonDecode(data));
     return session.token;
    }
    return null;
  }

  Future<int?> get accessUserId async{
    final data = await flutterSecureStorage.read(key: 'session');
    if(data!=null){
      final session = Session.fromJson(jsonDecode(data));
      return session.userId;
    }
    return null;
  }

  Future<void> saveSession(AuthenticationResponse authenticationResponse) async {
    final Session session = Session(authenticationResponse.id, authenticationResponse.accessToken, DateTime.now());
    final data = jsonEncode(session.toJson());
    await flutterSecureStorage.write(key: 'session', value: data);
  }

  Future<void> LogOut() async{
   await flutterSecureStorage.deleteAll();
  }

}