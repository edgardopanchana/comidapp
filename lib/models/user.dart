class User {
  User({
    required this.id,
    required this.email,
    required this.password,
    required this.telephone,
    required this.name,
    required this.lastName,
    required this.idRol,
    required this.aboutMe,
    required this.photo,
    required this.direction,
    required this.createdAt,
    required this.updatedAt,
  });

  final int id;
  final String email;
  final String password;
  final String telephone;
  final String name;
  final String lastName;
  final dynamic idRol;
  final String aboutMe;
  final String photo;
  final String direction;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    email: json["email"],
    password: json["password"],
    telephone: json["telephone"],
    name: json["name"],
    lastName: json["last_name"],
    idRol: json["id_rol"],
    aboutMe: json["about_me"],
    photo: json["photo"],
    direction: json["direction"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "email": email,
    "password": password,
    "telephone": telephone,
    "name": name,
    "last_name": lastName,
    "id_rol": idRol,
    "about_me": aboutMe,
    "photo": photo,
    "direction": direction,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
  };
}
