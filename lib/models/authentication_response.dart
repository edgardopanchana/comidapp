
class AuthenticationResponse {
  final int id;
  final String name;
  final String lastName;
  final String email;
  final String photo;
  final String accessToken;

  AuthenticationResponse(
      this.id,
      this.name,
      this.lastName,
      this.email,
      this.photo,
      this.accessToken
      );

  static AuthenticationResponse fromJson(Map<String, dynamic> json){
    return AuthenticationResponse(
        json['id'],
        json['name'],
        json['last_name'],
        json['email'],
        json['photo'],
        json['accessToken']
    );
  }

  static AuthenticationResponse toJson(Map<String, dynamic> json){
    return AuthenticationResponse(json['id'],json['name'], json['last_name'], json['email'],json['photo'], json['accessToken']);
  }

}