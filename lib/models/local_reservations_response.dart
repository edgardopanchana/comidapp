// To parse this JSON data, do
//
//     final localReservationResponse = localReservationResponseFromJson(jsonString);

import 'dart:convert';

import 'package:comidapp/models/reservation.dart';

LocalReservationResponse localReservationResponseFromJson(String str) => LocalReservationResponse.fromJson(json.decode(str));

String localReservationResponseToJson(LocalReservationResponse data) => json.encode(data.toJson());

class LocalReservationResponse {
  LocalReservationResponse({
    required this.status,
    required this.reservas,
  });

  final bool status;
  final List<Reservation> reservas;

  factory LocalReservationResponse.fromJson(Map<String, dynamic> json) => LocalReservationResponse(
    status: json["status"],
    reservas: List<Reservation>.from(json["reservas"].map((x) => Reservation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "reservas": List<dynamic>.from(reservas.map((x) => x.toJson())),
  };
}