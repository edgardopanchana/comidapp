
class Reservation {
  Reservation(
      this.persons,
      this.paid,
      this.message,
      this.active,
      this.createdAt,
      this.updatedAt,
      this.userId,
      this.eventId,
      this.foodId
      );

  final int persons;
  final bool paid;
  final String message;
  final bool active;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int userId;
  final int eventId;
  final int foodId;

  factory Reservation.fromJson(Map<String, dynamic> json) => Reservation(
    json["persons"],
    json["paid"],
    json["message"],
    json["active"],
    DateTime.parse(json["createdAt"]),
    DateTime.parse(json["updatedAt"]),
    json["userId"],
    json["eventId"],
    json["foodId"]
  );

  Map<String, dynamic> toJson() => {
    "persons": persons,
    "paid": paid,
    "message": message,
    "active": active,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "userId": userId,
    "eventId": eventId,
    "foodId": foodId
  };
}
