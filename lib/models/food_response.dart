// To parse this JSON data, do
//
//     final foodResponse = foodResponseFromJson(jsonString);

import 'dart:convert';

import 'package:comidapp/models/food.dart';

FoodResponse foodResponseFromJson(String str) => FoodResponse.fromJson(json.decode(str));

String foodResponseToJson(FoodResponse data) => json.encode(data.toJson());

class FoodResponse {
  FoodResponse({
    required this.status,
    required this.food,
  });

  final bool status;
  final Food food;

  factory FoodResponse.fromJson(Map<String, dynamic> json) => FoodResponse(
    status: json["status"],
    food: Food.fromJson(json["food"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "food": food.toJson(),
  };
}