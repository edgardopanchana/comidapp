import 'dart:convert';

import 'package:comidapp/models/user.dart';

UserResponse userResponseFromJson(String str) => UserResponse.fromJson(json.decode(str));

String userResponseToJson(UserResponse data) => json.encode(data.toJson());

class UserResponse {
  UserResponse({
    required this.status,
    required this.user,
  });

  final bool status;
  final User user;

  factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
    status: json["status"],
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "user": user.toJson(),
  };
}