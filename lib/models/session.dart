

class Session{
  final int userId;
  final String token;
  final DateTime createdAt;

  Session(this.userId, this.token, this.createdAt);

  static Session fromJson (Map<String, dynamic> json){
    return Session(
      json['id'],
        json['token'],
        DateTime.parse(json['createdAt']),
    );
  }

  Map<String, dynamic> toJson(){
    return {
      "id":this.userId,
      "token":this.token,
      "createdAt": this.createdAt.toString()
    };
  }


}