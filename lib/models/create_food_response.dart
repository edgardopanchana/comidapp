
class CreateFoodResponse {

  final bool status;
  final String message;
  final int foodId;

  CreateFoodResponse(
      this.status,
      this.message,
      this.foodId);

  static CreateFoodResponse fromJson(Map<String, dynamic> json){
    return CreateFoodResponse(
        json['status'],
        json['message'],
        json['foodId']
    );
  }
}