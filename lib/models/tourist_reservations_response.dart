import 'dart:convert';

import 'package:comidapp/models/reservation.dart';

TouristReservationResponse touristReservationResponseFromJson(String str) => TouristReservationResponse.fromJson(json.decode(str));

String touristReservationResponseToJson(TouristReservationResponse data) => json.encode(data.toJson());

class TouristReservationResponse {
  TouristReservationResponse(
    this.status,
    this.reservations,
  );

  final bool status;
  final List<Reservation> reservations;

  factory TouristReservationResponse.fromJson(Map<String, dynamic> json) => TouristReservationResponse(
    json["status"],
    List<Reservation>.from(json["reservations"].map((x) => Reservation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "reservations": List<dynamic>.from(reservations.map((x) => x.toJson())),
  };
}
