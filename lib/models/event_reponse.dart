import 'dart:convert';
import 'event.dart';

EventResponse eventResponseFromJson(String str) => EventResponse.fromJson(json.decode(str));

String eventResponseToJson(EventResponse data) => json.encode(data.toJson());

class EventResponse {
  EventResponse({
   required this.status,
   required this.event,
  });

  final bool status;
  final Event event;

  factory EventResponse.fromJson(Map<String, dynamic> json) => EventResponse(
    status: json["status"],
    event: Event.fromJson(json["event"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "event": event.toJson(),
  };
}
